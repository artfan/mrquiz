// Выбор первой кнопки, чтобы начать Квиз
document.querySelector(".start-quiz").addEventListener("click", function(){
	document.querySelector(".wrapper__first").classList.add("step--hidden");
	setTimeout(function(){
		document.querySelector(".wrapper__first").style.display = "none";
	}, 500);
	setTimeout(function(){
	document.querySelector(".wrapper__quiz").style.display = "flex";
	}, 500);
});

let stepBtn, prevBtn, quizCon, procents;

// Кнопка следующий шаг
stepBtn = document.querySelector(".btn--step");
stepBtn.addEventListener("click", nextStep);

// Кнопка предыдущий шаг
prevBtn = document.querySelector(".btn--step-prev");
prevBtn.addEventListener("click", prevStep);

// Контейнер квиза
quizCon = document.getElementById("quiz-con");
// Значения процентов
procents = document.querySelector(".label__procents");

// У кнопки есть параметр data-prev-предыдущий шаг, data-active - действующий шаг и data-next - следующий шаг
// При нажатии на кнопку идет переключение классов, согласно этим параметрам
function nextStep(){

	quizCon.classList.remove(stepBtn.dataset.active);
	quizCon.classList.add(stepBtn.dataset.next);
	stepBtn.disabled = true;
	scrollTo(0, 0);
	switch (stepBtn.dataset.next){
		case "quiz--second":
			prevBtn.disabled = false;
			stepBtn.dataset.prev = "quiz--first";
			stepBtn.dataset.active = "quiz--second";
			stepBtn.dataset.next = "quiz--third";
			procents.innerHTML = "17";
		break;
		case "quiz--third":
			stepBtn.dataset.prev = "quiz--second";
			stepBtn.dataset.active = "quiz--third";
			stepBtn.dataset.next = "quiz--fourth";
			procents.innerHTML = "33";
		break;
		case "quiz--fourth":
			stepBtn.dataset.prev = "quiz--third";
			stepBtn.dataset.active = "quiz--fourth";
			stepBtn.dataset.next = "quiz--fifth";
			procents.innerHTML = "50";
		break;
		case "quiz--fifth":
			stepBtn.dataset.prev = "quiz--fourth";
			stepBtn.dataset.active = "quiz--fifth";
			stepBtn.dataset.next = "quiz--six";
			procents.innerHTML = "67";
		break;
		case "quiz--six":
			stepBtn.dataset.prev = "quiz--fifth";
			stepBtn.dataset.active = "quiz--six";
			stepBtn.dataset.next = "last";
			procents.innerHTML = "83";
		break;
		case "last":
			document.querySelector(".quiz--final").style.display = "block";
		break;
	}
}
function prevStep(){
	quizCon.classList.remove(stepBtn.dataset.active);
	quizCon.classList.add(stepBtn.dataset.prev);
	stepBtn.disabled = false;
	switch (stepBtn.dataset.active){
		case "quiz--second":
			prevBtn.disabled = true;
			stepBtn.dataset.prev = "";
			stepBtn.dataset.active = "quiz--first";
			stepBtn.dataset.next = "quiz--second";
			procents.innerHTML = "0";
		break;
		case "quiz--third":
			stepBtn.dataset.prev = "quiz--first";
			stepBtn.dataset.active = "quiz--second";
			stepBtn.dataset.next = "quiz--third";
			procents.innerHTML = "17";
		break;
		case "quiz--fourth":
			stepBtn.dataset.prev = "quiz--second";
			stepBtn.dataset.active = "quiz--third";
			stepBtn.dataset.next = "quiz--fourth";
			procents.innerHTML = "33";
		break;
		case "quiz--fifth":
			stepBtn.dataset.prev = "quiz--third";
			stepBtn.dataset.active = "quiz--fourth";
			stepBtn.dataset.next = "quiz--fifth";
			procents.innerHTML = "50";
		break;
		case "quiz--six":
			stepBtn.dataset.prev = "quiz--fourth";
			stepBtn.dataset.active = "quiz--fifth";
			stepBtn.dataset.next = "quiz--six";
			procents.innerHTML = "67";
		break;		
	}
}
// Обработчик radio и checkbox, чтобы без выбранного значения нельзя было перейти на следующий шаг
let step1 = document.getElementsByName("quest1"),
step2 = document.getElementsByName("quest2"),
step3 = document.getElementsByName("quest3"),
step4 = document.getElementsByName("quest4"),
step5 = document.getElementsByName("quest5"),
step6 = document.getElementsByName("quest6");
validate (step1);
validate (step2);
validate (step3);
validate (step4);
validate (step5);
validate (step6);
function validate(step){
	for (var i = 0; i < step.length; i++) {
		step[i].addEventListener("click", function() {
			console.log(stepBtn.dataset.active + " .btn--step");
			document.querySelector("." + stepBtn.dataset.active + " .btn--step").disabled = false;
		});
	} 
}