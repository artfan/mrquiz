	const gulp  = require("gulp"),
sass = require("gulp-sass"),
browserSync = require("browser-sync"),
autoprefixer = require("gulp-autoprefixer");

gulp.task("browser", function(){
	browserSync.init({
		server: {
			baseDir: "src/"
		}
	});
});
gulp.task("sass", function(){
	return gulp.src("src/scss/*.scss")
	.pipe(sass().on("erorr", sass.logError))
	.pipe(autoprefixer({
		overrideBrowserslist: ["last 8 versions"]
	}))
	.pipe(gulp.dest("src/css"))
	.pipe(browserSync.reload({stream: true}))
});
gulp.task("html", function(){
	return gulp.src("src/*.html")
	.pipe(browserSync.reload({stream: true}))
});
gulp.task("js", function(){
	return gulp.src("src/js/*.js")
	.pipe(browserSync.reload({stream: true}))
})
gulp.task("watch", function(){
	gulp.watch("src/*.html", gulp.parallel("html"));
	gulp.watch("src/scss/*.scss", gulp.parallel("sass"));
	gulp.watch("src/js/*.js", gulp.parallel("js"));
})
gulp.task("dev", gulp.parallel("sass", "html", "js", "browser","watch"));

gulp.task("build-html", function(){
	return gulp.src("src/*.html")
	.pipe(gulp.dest("../result"))
})
gulp.task("build-css", function(){
	return gulp.src("src/scss/*.scss")
	.pipe(sass().on("erorr", sass.logError))
	.pipe(autoprefixer({
		overrideBrowserslist: ["last 8 versions"]
	}))
	.pipe(gulp.dest("../result/css"))
})
gulp.task("build-js", function(){
	return gulp.src("src/js/*.js")
	.pipe(gulp.dest("../result/js"))
})
gulp.task("build", gulp.parallel("build-html", "build-css", "build-js"));
